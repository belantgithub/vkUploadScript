# -*- coding: utf-8 -*-
import vk_api
import requests
import sys
import argparse
from datetime import datetime


def create_parser ():
    parser = argparse.ArgumentParser(
        prog='VK WallPost Script',
        description='''Скрипт предназначен для публикации сообщений с изображениями на стене сообщества.''',
        epilog='''(c) belant 2018 '''
    )
    parser.add_argument('-l', '--login', help='Логин или номер телефона от учетной записи в VK (лучше номер телефона)')
    parser.add_argument('-p', '--passw', help='Пароль', metavar='PASSWORD')
    parser.add_argument('-a', '--app', type=int, help='ID standalone-приложения')
    parser.add_argument('-t', '--token', help='Access-token')
    parser.add_argument('-g', '--group', type=int, help='ID сообщества (без "-"')
    parser.add_argument('-d', '--date', help='Дата публикации в формате (гггг-мм-ддTчч:мм:сс)', type=str)
    parser.add_argument('-m', '--message', default='trampampam', type=str, help='Сообщение')
    parser.add_argument('-i', '--images', nargs='+', help='Список ищображений')

    return parser


def get_vk_session(login, passw, app):
    vk_session = vk_api.VkApi(login=login, password=passw, app_id=app)
    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    return vk_session


def main():
    parser = create_parser()
    namespace = parser.parse_args()
    if namespace.token is None:
        vk_session = get_vk_session(namespace.login, namespace.passw, namespace.app)
    else:
        vk_session = vk_api.VkApi(token=namespace.token, app_id=namespace.app)
    vk = vk_session.get_api()

    upload = vk_api.VkUpload(vk_session)
    vk_img_list = []
    for img in namespace.images: #TODO: нужно сделать разделение загрузки по 6 фото
        image = upload.photo_wall(
            img,
            group_id=namespace.group,
        )
        vk_img_url = 'photo{}_{}'.format(
            image[0]['owner_id'], image[0]['id']
        )
        vk_img_list.append(vk_img_url)
    vk_img_string = ', '.join(vk_img_list)

    mes = namespace.message
    mes = str.replace(mes, '\\r\\n', '\n')
    mes = str.replace(mes, '\\n', '\n')
    mes = str.replace(mes, '\\t', '\t')

    if namespace.date is not None:
        udate = datetime.strptime(namespace.date, '%Y-%m-%dT%H:%M:%S+03:00')
        vk.wall.post(
            owner_id="-{}".format(namespace.group),
            message=mes,
            publish_date=udate.timestamp(),
            attachments=vk_img_string)
    else:
        vk.wall.post(
            owner_id="-{}".format(namespace.group),
            message=mes,
            attachments=vk_img_string)


if __name__ == '__main__':
    main()
